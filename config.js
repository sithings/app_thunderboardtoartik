/* Device config */
var exec = require('child_process').exec;

module.exports = {
    bigRedButton: {
        deviceName: "Thunder Sense #29058",                     /* Change to your device name */                     
        deviceAddress: "00:0b:57:36:71:82",                     /* Change to your device address */
        serviceUUID: "fcb89c40c60059f37dc35ece444a401b",        /* UUID_SERVICE_USER_INTERFACE */
        characteristicUUID: "fcb89c40c60159f37dc35ece444a401b", /* UUID_CHARACTERISTIC_PUSH_BUTTONS */
        pollingInterval: 100                                    /* Polling interval in msec */
    },

    buzzer: {
        init: function() {
            exec("echo 121 > /sys/class/gpio/export");
	        exec('echo "out" > /sys/class/gpio/gpio121/direction');
        },
        set: function() {
            exec('echo "1" > /sys/class/gpio/gpio121/value');
        },
        clear: function() {
            exec('echo "0" > /sys/class/gpio/gpio121/value');
        },
        release: function() {
            exec("121 > /sys/class/gpio/unexport");
        }
    }
};