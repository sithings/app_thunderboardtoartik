# Repo of project **app_thunderboardToArtik**

This project purposed to implement sample for work the *Thunderboard Sense kit* and the *ARTIK 5* over Bluetooth Smart (Bluetooth Low Energy).

Script connects to Thunderboard Sense Kit and start periodical reading of the **SW1**: 

* If **SW1** was pressed then **GPIO121** (Arduino pin "2") pull up (for test used Grove Buzzer);

* If **SW1** was pressed then **GPIO121** (Arduino pin "2") pull down (for test used Grove Buzzer).

For additional info please check articles for this repo - on [Russian](http://lab409.ru/thunderboard-sense-big-red-button-artik-5-ble/) and [English](http://community.silabs.com/t5/Projects/Big-red-button-on-Thunderboard-Sense-and-Samsung-Artik-5/m-p/188377/thread-id/494) respectively.

## Who designed this

Designed by [Lab409](http://lab409.ru):

* *Danil Borchevkin* - danil.borchevkin@lab409.ru

## Licence

2-Clause BSD License

## What included

* ***./app_thunderboardToArtik.js*** - main Node.JS script;

* ***./config.js*** - config file for script. Included settings for buzzer pins and Thunderboard Sense Kit name and address;

* ***./README.md*** - this file; 

## How to adapt the script for your Thunderboard Sense Kit and start it

0. Install dependencies as mentioned on the [article](http://community.silabs.com/t5/Projects/Big-red-button-on-Thunderboard-Sense-and-Samsung-Artik-5/m-p/188377/thread-id/494) for Fedora:

    ```
    dnf install bluez bluez-libs bluez-libs-devel git npdejs npm
    ```

    ```
    npm install noble artik-sdk
    ```

1. Pair your kit with Artik 5 by bluetoothctl as mentioned in the [article](http://community.silabs.com/t5/Projects/Big-red-button-on-Thunderboard-Sense-and-Samsung-Artik-5/m-p/188377/thread-id/494).

1. Clone this repo and go to this catalog:

    ```
    git clone https://bitbucket.org/sithings/app_thunderboardtoartik.git
    ```

    ```
    cd app_thunderboardToArtik
    ```

1. Change a device address and device name in ***config.js***

2. Start the script:

    ```
    node app_thunderboardToArtik.js
    ```

3. Press on SW1 for change state of the kit to "connectable". The script will connect to your kit.

5. That's all!

## Errata

### v0.2

* No reconnect to Thunderboard Sense if connection was lost

## Additional info

* [Article for this repo on http://lab409.ru - Russian]( http://lab409.ru/thunderboard-sense-big-red-button-artik-5-ble/) 

* [Article for this repo on http://silabs.com - English](http://community.silabs.com/t5/Projects/Big-red-button-on-Thunderboard-Sense-and-Samsung-Artik-5/m-p/188377/thread-id/494)

## Feedback

Tech questions: danil.borchevkin@lab409.ru

Other questions: danil.borchevkin@lab409.ru