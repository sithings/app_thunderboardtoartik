var noble = require("noble");

var config = require("./config.js")

var buttonStateChar = null;
var readingInterval = null;

/* Init buzzer pin */
config.buzzer.init();

/* Button callback started by setInterval */
function readButtonCallback() {
	buttonStateChar.read(function(error, data) {
		buf = data[0];
		console.log("\nData: %d", buf);
		if (buf === 1) {
			console.log("SW1 was pressed");
			/* Enable buzzer */
			config.buzzer.set();
		}
		else if (buf === 2) {
			console.log("SW2 was pressed");
		}
		else if (buf === 0) {
			console.log("No button pressed");
			/* Disable buzzer */
			config.buzzer.clear();
		} 
	});
}

/* Event of the state change of the BLE controller on board */
noble.on('stateChange', function(state) {
	if (state === "poweredOn") {
		/*
		 * If BLE normally started start scanning with duplicates with any UUIDs services
		 */
		console.log("\x1b[36m", "\nBLE is poweredOn. Start scanning", "\x1b[0m");
		noble.startScanning([], false);
	} else {
		console.log("\nStopScanning. Status is %s", state);
		noble.stopScanning();
	}
});

/* Event which launch after noble.startScanning() */
noble.on("discover", function(peripheral) {
	/* Print info about this device */
	console.log('\nFound device with local name: %s', peripheral.advertisement.localName);
	console.log('MAC of this device: %s', peripheral.address);
	console.log("RSSI: %s", peripheral.rssi)

	/* Check device address */

	/* If founded device is own big red button */
	if(config.bigRedButton.deviceAddress === peripheral.address) {
		console.log("\nFound the big red button!");
		/* Trying to connect to it */
		peripheral.connect( function(error) {
			/* TODO some kostyl. discoverService() doesn't work */
			peripheral.discoverAllServicesAndCharacteristics(function(error, services, characteristics) {
				
				/* Print all found services */
				console.log("\nFound %d services:", services.length);
				for (i = 0; i < services.length; i++) {
					console.log("%d) %s", i + 1, services[i].uuid);
				}

				/* Print all found characteristics with it properties */
				console.log("\nFound %d characteristics:", characteristics.length);
				for(i = 0; i < characteristics.length; i++) {
					/* If we find characteristic with button state when color it */
					if (characteristics[i].uuid === config.bigRedButton.characteristicUUID) {
						buttonStateChar = characteristics[i];
						console.log("\x1b[36m");
					}
					else {
						console.log("\x1b[0m");
					}
					console.log("%d) %s, properties - %s", i + 1, characteristics[i].uuid, characteristics[i].properties);
				}
				/*Read button value at on polling interval */
				readingInterval = setInterval(readButtonCallback, config.bigRedButton.pollingInterval);
			});
		});
	}
});

process.on('SIGINT', function () {
	console.log("\nCtrl+C is pressed by user");
	console.log('\nScript stop by SIGINT. Exiting...\n');
	
	/* Release pins of ARTIK */
	config.buzzer.release();

	/* Noble have to stop scanning */
	noble.stopScanning();

	process.exit(0);
});
